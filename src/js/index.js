import Like from './models/Likes';
import Recipe from './models/Recipe';
import Search from './models/Search';
import ShoppingList from './models/ShoppingList';
import * as shoppingListView from './views/shoppingListView';
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import { domElements, clearLoadingSpinner, renderLoadingSpinner } from './views/base';
import Likes from './models/Likes';


/*
global state of app
-Search object
-Current recipe object
-Shopping list object
-Liked recipes
*/
const applicationState = {}; //state
window.applicationState = applicationState;

                                                              //Event Handler

//getResults() is a promise -> we need to mark this fn() w/ async
const searchController = async () => { // controlSearch

    //grab search query from view
    const query = searchView.getSearchInput();

      //if query -> create search object => add to state
      if(query){
          applicationState.search = new Search(query);

          //clear search input
          searchView.clearSearchInput();

          //clear search results
          searchView.clearSearchResults();

          //render spinning loader
          renderLoadingSpinner(domElements.searchResults);

          try{
            //search for Recipes
            await applicationState.search.getResults();

            //clear spinning loader
            clearLoadingSpinner();

            // render results from query
            searchView.renderResults(applicationState.search.result);
          }
          catch(error)
          {
             alert('Error in retrieving recipe instructions');
             clearLoadingSpinner();
          }
        }
    }

    const recipeController = async () => { //controlRecipe
        //window.location = url, hash -> hash at the end, replace -> remove the '#' sign
        const recipeIdHash = window.location.hash.replace('#', ' '); 

        if(recipeIdHash)
        {
          //clear UI
          recipeView.clearRecipe();
          renderLoadingSpinner(domElements.recipeIngredientsList);

          //create new recipe object
          applicationState.recipe = new Recipe(recipeIdHash);
          

          try{
              //retrieve recipe instruction details and parse ingredients
              await applicationState.recipe.getRecipeInstructions();
              applicationState.recipe.parseIngredients();

              //calculate servings and cook time
              applicationState.recipe.getCookTime();
              applicationState.recipe.getServingsAmount();

              //render recipe
              clearLoadingSpinner();
              recipeView.renderRecipe(applicationState.recipe);
          }
          catch(error){
            alert(error);
          }
        }
    };

    const updateQuantityInUI = () => { // controlList

      //if no list exists - create list

      if(!state.list) 
        state.list = new ShoppingList();

        //add each ingredient to list then UI
      applicationState.recipe.ingredientList.forEach(ingredient => {
       const ingredientAddedTolist = state.list.addIngredientToList(
          ingredient.quantity,
          ingredient.unit,
          ingredient.ingredient
        );
      });

      shoppingListView.renderNewIngredient(ingredientAddedTolist);
    }


                                        //likeController  
     
  //Adds or removes like from Likes List 
     const likeController = () => {
      if(!applicationState.likes)
            applicationState.likes = new Likes();

      const currentRecipeID = applicationState.recipe.itemId;

      if(applicationState.likes.isRecipeLiked(currentRecipeID)){  //like exists
        applicationState.likes.deleteLikeFromList(currentRecipeID); // remove like from model

      } 
      else 
          const newLike = applicationState.likes.addLikeToList(   //like doesn't exists -> add like
                currentRecipeID, 
                applicationState.recipe.title,
                applicationState.recipe.author,
                applicationState.recipe.imageUrl); 
                
                console.log(applicationState.likes);
                
     };

    //handle delete and update list item events
    domElements.shoppingList.addEventListener('click', e => {
        const itemId = e.target.closest('.shopping_item').dataset.itemId;

        //handle delete event
        if (e.target.closest('.shopping__delete, .shopping__delete *')){

            //delete from app state
            applicationState.list.deleteIngredientFromList(itemId);

            //delete from UI
            shoppingListView.removeItemFromShoppingList(itemId);
        }

        //handle update event
        if(e.target.closest('.shopping__count-value')){

          //retrieve new quantity
          const newQuantity = parseInt(e.target.value);

          //update quantity in app state
          applicationState.list.updateIngredientQuantity(itemId, newQuantity);
          
        }

    });






                                                        //Event Listener
      
//submit button                                                        
domElements.searchForm.addEventListener('submit', e => {
  e.preventDefault(); // don't refresh the page
  searchController();
});

//recipe selected
window.addEventListener('hashchange', recipeController);

window.addEventListener('load', recipeController);

//recipe quantity updated - increae/decreae button
domElements.recipeIngredientsList.addEventListener('click', e => {
  if(e.target.matches('.btn-decrease, .btn-decrease *')) // ingredient list quantity increase
  {
      if(applicationState.recipe.servings > 1)
      {
           applicationState.recipe.updateServings('decrease'); // decrease count internally
           recipeView.updateServingsQuantity(applicationState.recipe); // update quantity in UI
      }
      else if(e.target.matches('.btn-increase, .btn-increase *')) // ingredient list quantity decrease
      {
            applicationState.recipe.updateServings('increase'); // increase quantity internally
            recipeView.updateServingsQuantity(applicationState.recipe); // update quantity in UI
      }
      else if(e.target.matches('.recipe__btn--add, .recipe__btn--add *'))// shopping list - ingredient quantity increase
      {
        console.log("Add button clicked");
          updateQuantityInUI();
      } 
      else if(e.target.matches('.recipe__love, .recipe__love *')){
        likeController();
      }

  }
  console.log(applicationState.recipe);  
});

window.ingredientList = new ShoppingList();

//Same code as above but more efficient and cleaner
//['hashchange', 'load'].forEach(event => window.addEventListener(event, renderReceipeInstructions));

 
  