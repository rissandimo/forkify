import uniqid from 'uniqid';

export default class ShopppingList{
    constructor(){
        this.ingredients = [];
    }

    addIngredientToList(quantity, unit, ingredient){
        const newIngredient = {
            id: uniqid(),
            quantity,
            unit,
            ingredient
        };
        this.ingredients.push(newIngredient);
        return newIngredient;
    }

    //Remove item from list
    deleteIngredientFromList(idOfItemToDelete){
        const indexOfItemToDelete = this.ingredients.findIndex(element => element.id === idOfItemToDelete);
        this.ingredients.splice(indexOfItemToDelete, 1);
    }

    updateIngredientQuantity(idOfItemToChangeQuantity, newQuantity){
        this.ingredients.find(element => element.id === idOfItemToChangeQuantity).quantity = newQuantity;
    }
}