export default class Likes{

    constructor(){
        this.likes = [];
    }

    addLikeToList(recipeId, title, author, image){
        const like = { recipeId, title, author, image };
        this.likes.push(like);
        return like;
    }

    deleteLikeFromList(recipeIdToDelete){
        const indexOfLikeToDelete = this.likes(like => like.recipeId === recipeIdToDelete);
        
        this.likes.splice(indexOfLikeToDelete, 1);
    }

    isRecipeLiked(recipeId){
        //check if see if in the likes array - if there is a recipeId that was passed in
        //if an index is returned -> like exists; otherwise it doesn't
        return this.likes.findIndex(recipe => recipe.id === recipeId) !== -1;
    }

    getNumberOfLikes(){
        return this.likes.length;
    }
}
