import axios from 'axios';

export default class Recipe {

    constructor(recipeId){
        this.recipeId = recipeId;
    }

    async getRecipeInstructions(){
        try{
        const recipeResult = await axios(`https://forkify-api.herokuapp.com/api/get?rId=${this.recipeId}`); // return promise
        
        this.title = recipeResult.data.recipe.title;
        this.author = recipeResult.data.recipe.publisher;
        this.imageUrl = recipeResult.data.recipe.image_url;
        this.ingredients = recipeResult.data.recipe.ingredients;
    }
        catch(error){
            console.log(error);            
            alert('Error in retreiving recipe instructions');
        }
    }

    //every 3 ingredients - 1 period => 15 minutes
    // 3 ingredients = 15 mins, 6 ingredients = 30 mins, etc..
     getCookTime(){
        const numOfIngredients = this.ingredients.length; // array
        const periods = Math.ceil(numOfIngredients / 3);
        this.cookTimeInMinutes = periods * 15;
    }

    getServingsAmount(){
        this.servings = 4;
    }

    /*
    Create an array of various units names - cups, cup, teaspoon, teaspoon
    Create an array of various uniformed/standard unit names - cup, tsp, tbsp, oz
    Traverse the Recipe.ingredients array and store it in an array called 'newIngredients'
    For every ingredient in the array:
    -convert the ingredient to lower case
    -traverse the lower case ingredient and convert any random units to a standaord unit. Ex 'teaspoon' -> 'tsp'
    -remove parenthesis - But I believe it needs to be quotations
    -split the entire ingredient into an array
    -Traverse the ingredient array and check if any of the words are a unit of measure - if so return the index 
    in the ingredient array
    Assign the array of ingredient to the Recipe.ingredient array property
    */
    parseIngredients () {

        //array of 'not uniformed units', array of 'uniformed units'
        const incorrectUnitArray = ['tablespoons', 'tablespoon', 'ounces', 'ounce', 'teaspoons', 'teaspoon', 'cups', 'pounds'];
        const correctUnitArray = ['tbsp', 'tbsp', 'oz', 'oz', 'tsp', 'tsp', 'cup', 'pound'];
        const correctUnitArray2 = [...correctUnitArray, 'kg', 'g'];

        //traverse Recipe.ingredients array and store them in new ingredients array
        //there will be a bunch of new ingredients - originalIngredient is the name we have given for the first 'element'
        //map takes a function - so instead of passing a function, we are coding our function manually
        const newIngredients = this.ingredients.map(currentIngredient => {
                
        //get access to new ingredient, and make it lowercase
        let ingredient = currentIngredient.toLowerCase();

        /*Traverse the unitsNotUniformed array - pass in the incorrect unit/string and its index.
        We have to traverse the incorrect array, b/c in the replace method, the first arg is what you
        want to change, followed by the correct/new argument.
        For each of the incorrect units in the array - apply it to the lower case ingredient
        Replace the ingredient's incorrect unit name to the correct name. For some units there are multiple
        types such as 'tablespoons' and 'tablespoon' which both will get converted to 'tbsp'. And others
        such as 'cups' that will get converted to 'cup'.
        */
        incorrectUnitArray.forEach( (incorrectUnit, currentIndex) => {
            //NEW ingredient - replace incorrect unit with correct unit where applicable
            ingredient = ingredient.replace(incorrectUnit, correctUnitArray[currentIndex]);
        });

        //remove parenthesis from ingredient unit. Ex (2.5 oz) -> 2.5 oz
        //arbritary amount of spaces - ( - arbtritary n of chars ) - arbritary amount of spaces
        //Ex [pizza     (adfjk!!!!#*$*)       ] -> pizza
        ingredient = ingredient.replace(/ *\([^)]*\) */g, '');


                                    //parse ingredient into count, unit and ingredients

        //convert current ingredient to array
        const ingredientArray = ingredient.split(' ');

        //compare ingredient array to unit uniformed array
        //ingredient array = entire ingredient seperated out
        //element = element IN the 'ingredientArray'
        //search within ingredient array for any elements that are a unit(unitsUniformedArray)
        //return the index of that unit - ex 'cup' 'oz', 'tsp'
        //tldr: check if any of the elements in the ingredient array are a unit of measure - ex cup, oz, tsp
        const unitIndex = ingredientArray.findIndex(element => correctUnitArray2.includes(element));

        //check if a unit exists, then check if a quantity exists
        let ingredientObject;
        if(unitIndex > -1) // unit exists - 'oz', 'tsp'
        {
            //Ex ['4 1/2', 'cups'] -> ['4 1/2']
            //Ex ['3', 'cups'] -> ['3']
            const unitQuantityArray = ingredientArray.slice(0, unitIndex); // retrieve entire unit number .Ex '4 1/2'

            let unitQuantity = 0;
            if(unitQuantityArray.length === 1) // check if quantity is an integer
                unitQuantity = eval(unitQuantityArray[0].replace('-', '+')); // assign integer to quantity
            else // quantity is a fraction -> convert fraction to decimal - then assign to quantity
                unitQuantity = eval(unitQuantityArray.slice(0, unitIndex).join('+')); // convert 4 1/2 -> 4.5

            ingredientObject = {
                unitQuantity,
                unit: ingredientArray[unitIndex],
                ingredient: ingredientArray.slice(unitIndex + 1).join(' ')
            };


        }
        else if(parseInt(ingredientArray[0], 10))// no unit found - but check if there is a quantity - Ex - '1' bread
        {
            ingredientObject = {
                unitQuantity: parseInt(ingredientArray[0], 10), // first element(quantity) in ingredient array
                unit: '',
                ingredient: ingredientArray.slice(1).join(' ')
            }
        }
        else if(unitIndex == -1) // no unit or quantity
        {
            ingredientObject = {
                unitQuantity: 1, // Ex 1 tomatoe sauce 
                unit: '',
                ingredient // property will automatically get created and assigned to it
            }
        }

        return ingredientObject; // need to return it b/c using a map
        }); // end of traversal

        this.ingredients = newIngredients; // assign new ingredients array to Recipe.ingredients array
    }

    updateServings(quantityOption){

        const updatedServingQuantity = quantityOption === 'decrease' ? this.servings - 1 : this.servings + 1;

        this.ingredients.forEach(ingredient => {
            ingredient.unitQuantity *= (updatedServingQuantity / this.servings);
        });

        this.servings = updatedServingQuantity;

    }
}