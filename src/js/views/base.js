//reusable code

export const domElements = {
  likes: document.querySelector('.likes'),
  searchForm: document.querySelector('.search'),
  searchInput: document.querySelector('.search__field'),
  searchResults: document.querySelector('.results'),
  searchResultsList: document.querySelector('.results__list'),
  recipeIngredientsList: document.querySelector('.recipe'),
  shoppingList: document.querySelector('.shopping__list')
};

const cssElements = {
  loader: 'loader'
};

export const clearLoadingSpinner = () => { // clearLoader
    const spinningLoader = document.querySelector(`.${cssElements.loader}`);
    if(spinningLoader)
      spinningLoader.parentElement.removeChild(spinningLoader);
};

/*
pass in Parent (results class)
svg - scalable vector graphics - used to define graphics for the web
*/
export const renderLoadingSpinner = parent => {
  //loader - css class, rotation done by css animation attribute
  const spinningLoader = `
      <div class="loader">
        <svg>
          <use href="img/icons.svg#icon-cw"></use>
        </svg>
      </div>
    `;

    parent.insertAdjacentHTML('afterbegin', spinningLoader);
};