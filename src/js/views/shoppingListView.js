import { domElements } from './base';

export const renderNewIngredient = item => {
    const shoppingListMarkup = 
    `
            <li class="shopping__item data-itemId=${item.id}">
            <div class="shopping__count">
                <input type="number" value="${item.quantity}" step="${item.quantity}" class="shopping__count-value">
                <p>${item.unit}</p>
            </div>
            <p class="shopping__description">${item.ingredient}</p>
            <button class="shopping__delete btn-tiny">
                <svg>
                    <use href="img/icons.svg#icon-circle-with-cross"></use>
                </svg>
            </button>
        </li>
    `;

    domElements.shoppingList.insertAdjacentHTML('beforeend', shoppingListMarkup);
    console.log("renderNewIngredient()");
    
};

export const removeItemFromShoppingList = id => {
    const itemToDelete = document.querySelector(`[data-itemId="${id}"]`);
    if(itemToDelete)
        itemToDelete.parentElement.removeChild(itemToDelete);
};