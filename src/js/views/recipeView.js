import { Fraction } from 'fractional'; 
import { domElements } from './base';

export const clearRecipe = () => {
    domElements.recipeIngredientsList.innerHtml = '';
};

const formatQuantity = quantity => {
    if(quantity) // check if quantity exists - sometimes its not applicable
    {
        // get the quantity (2.5), convert it to string, then seperate them, then for each value:
        // - if integer (1 bread) -> return integer
        //if incomplete fraction (0.5 cups) -> return 0.5
        //if whole fraction (1.25 cups) -> return 1 1/2 
        const [integer, decimal] = quantity.toString().split('.').map(number => parseInt(number, 10));

        if(!decimal) return integer; // no decimal -> return integer

        if(integer === 0) // 0.5
        {
            const fractional = new Fraction(quantity);
            return `${fractional.numerator}/${fractional.denominator}`; // return fraction
        }
        else{
            fraction = Fraction(quantity - integer); //2.5 - 2 -> .5 -> 1/2
            return `${integer} ${fractional.numerator}/${fractional.denominator}`; //return (1 1/2)
            //NOTE - we are not doing division - we are just declaring the format `return integer numerator/denominator`
        }
    }
    return 'N/A';
};

export const createIngredient = ingredient => `
    <li class="recipe__item">
        <svg class="recipe__icon">
            <use href="img/icons.svg#icon-check"></use>
        </svg>
        <div class="recipe__count">${formatQuantity(ingredient.unitQuantity)}</div>
        <div class="recipe__ingredient">
            <span class="recipe__unit">${ingredient.unit}</span>
            ${ingredient.ingredient}
        </div>
    </li>
`;

export const renderRecipe = recipe => {
    const recipeItemMarkup = `
        <figure class="recipe__fig">
        <img src="${recipe.image}" alt="${recipe.title}" class="recipe__img">
        <h1 class="recipe__title">
            <span>${recipe.title}</span>
        </h1>
    </figure>
    <div class="recipe__details">
        <div class="recipe__info">
            <svg class="recipe__info-icon">
                <use href="img/icons.svg#icon-stopwatch"></use>
            </svg>
            <span class="recipe__info-data recipe__info-data--minutes">${recipe.cookTimeInMinutes}</span>
            <span class="recipe__info-text"> minutes</span>
        </div>
        <div class="recipe__info">
            <svg class="recipe__info-icon">
                <use href="img/icons.svg#icon-man"></use>
            </svg>
            <span class="recipe__info-data recipe__info-data--people">${recipe.servings}</span>
            <span class="recipe__info-text"> servings</span>

            <div class="recipe__info-buttons">
                <button class="btn-tiny btn-decrease">
                    <svg>
                        <use href="img/icons.svg#icon-circle-with-minus"></use>
                    </svg>
                </button>
                <button class="btn-tiny btn-increase">
                    <svg>
                        <use href="img/icons.svg#icon-circle-with-plus"></use>
                    </svg>
                </button>
            </div>

        </div>
        <button class="recipe__love">
            <svg class="header__likes">
                <use href="img/icons.svg#icon-heart-outlined"></use>
            </svg>
        </button>
    </div>

    <div class="recipe__ingredients">
        <ul class="recipe__ingredient-list">
            ${recipe.ingredients.map(recipe => createIngredient(recipe)).join('')}
        </ul>

        <button class="btn-small recipe__btn recipe__btn--add">
            <svg class="search__icon">
                <use href="img/icons.svg#icon-shopping-cart"></use>
            </svg>
            <span>Add to shopping list</span>
        </button>
    </div>

    <div class="recipe__directions">
        <h2 class="heading-2">How to cook it</h2>
        <p class="recipe__directions-text">
            This recipe was carefully designed and tested by
            <span class="recipe__by">${recipe.author}</span>. Please check out directions at their website.
        </p>
        <a class="btn-small recipe__btn" href="${recipe.url}" target="_blank">
            <span>Directions</span>
            <svg class="search__icon">
                <use href="img/icons.svg#icon-triangle-right"></use>
            </svg>

        </a>
    </div>
        `;

        domElements.recipeIngredientsList.insertAdjacentHTML('afterbegin', recipeItemMarkup);
};


export const updateServingsQuantity = recipe => {

    //update recipe quantity
    document.querySelector('.recipe__info-data--people').textContent = recipe.servings;

    //update ingredients quantity
    const currentUI_IngredientQuantities = Array.from(document.querySelectorAll('.recipe__count'));
    currentUI_IngredientQuantities.forEach((ingredient, index) => {
        ingredient.textContent = formatQuantity(recipe.ingredients[index].unitQuantity); //update UI quantity w/ new updated quantity
    });
    
};