import { domElements} from './base';

export const clearSearchInput = () => {
    domElements.searchInput.value = "";
}

export const clearSearchResults = () => {
    domElements.searchResultsList.innerHTML = '';
};

//this function statement is one line -> return is implicit
export const getSearchInput = () => domElements.searchInput.value; // getInput()

const renderRecipeToUI = recipe => {
    const recipeDomElement = `
    <li>
    <a class="results__link" href="#${recipe.recipe_id}">
        <figure class="results__fig">
            <img src="${recipe.image_url}" alt="${recipe.title}">
        </figure>
        <div class="results__data">
            <h4 class="results__name">${limitRecipeTitle(recipe.title)}.</h4>
            <p class="results__author">${recipe.publisher}</p>
        </div>
    </a>
</li>
    `;

    domElements.searchResultsList.insertAdjacentHTML('beforeend', recipeDomElement);
};

/*recipeCharLimit = 17 (default parameter) - if limit not specified -> default is 17(recommended)
Ex
'Pasta with tomato and spinach'
currentCharCount (0) + currentWord.length(5) = 5 <= 17 => ['Pasta']
currentCharCount (5) + currentWord.length(4) = 9 <= 17 => ['Pasta', 'with'] 
currentCharCount (9) + currentWord.length(6) = 15 <= 17 => ['Pasta', 'with', 'tomato'] 
currentCharCount (15) + currentWord.length(3) = 18 <= 17 X ['Pasta', 'with', 'tomato'] - OFFICIAL 
*/
const limitRecipeTitle = (title, titleCharLimit = 17) => {
    //check if title length exceeds limit
    if(title.length > titleCharLimit){
    const updatedTitle = [];
        //split title to array
        title.split(' ').reduce((currentCharCount, currentWord) => {
            if( (currentCharCount + currentWord.length) <= titleCharLimit){
                updatedTitle.push(currentWord);
            }
            return currentCharCount + currentWord.length;
             // update accumulator/memory - we have to tell reduce how to function/update memory
             // 0 + 5(total count)
             // 5 + 4 = 9 (total count)
             // 9 + 6 = 15 (total acount)
        }, 0);

        //return array (new title), join(' ') -> adds spaces, ... -> Pasta with tomato...
        return `${updatedTitle.join(' ')}...`;
    }
    return title; // return original title
};

//array of 30 recipes
//10 results per page
//page starts at 1 unless otherwise clicked
//for each 10 recipes in array - render them to UI
export const renderResults = (recipes, page = 1, resultsPerPage = 10) => {
                                
                                //starting page
                    //Current Page (1-1) -> 0(page number)
                    //Current Page (2-1) -> 1(page number)
                    //Current Page (3-1) -> 2(page number)
    const startingPage = (page - 1) * resultsPerPage;

                                //ending page
                    //1 * 10 -> 10, ending page = 10
                    //2 * 10 -> 20, ending page = 20
                    //3 * 10 -> 30, ending page = 30
    const endingPage = page * resultsPerPage;

    //retrieve portion of results
    recipes.slice(startingPage, endingPage).forEach(renderRecipeToUI);
};
